import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

//mport components
import MainSite from './components/MainSite.vue'
import Register from './components/Register.vue'
import Users from './components/Users.vue'
import Login from './components/Login.vue'
import InternalMenu from './components/InternalMenu.vue'
import Groups from './components/Groups.vue'
import NewGroup from './components/NewGroup.vue'
import LogToGroup from './components/LogToGroup.vue'
import Group from './components/Group.vue'

Vue.config.productionTip = false
Vue.use(VueRouter)

const routes = [
  {path: '/', component: MainSite },
  {path: '/register', component: Register},
  {path: '/users', component: Users},
  {path: '/login', component: Login},
  {path: '/internalMenu', component: InternalMenu},
  {path: '/groups', component: Groups},
  {path: '/newGroup', component: NewGroup},
  {path: '/logToGroup', component: LogToGroup},
  {path: '/group', component: Group}
]

const router = new VueRouter({
  mode: 'history',
  routes: routes
})

new Vue({
  router: router,
  render: h => h(App),
}).$mount('#app')
